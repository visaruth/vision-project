# -*- coding: utf-8 -*-
"""Vision Project.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1A4PI4hj8Kb-HXgRLaMzeg-hDCvfwmarb
"""

import numpy as np
import matplotlib.patches as mpatches
import cv2
import skimage
from skimage import io,morphology,future
import matplotlib.pyplot as plt
from skimage.measure import label, regionprops

from skimage.filters import threshold_otsu
from skimage.segmentation import clear_border
from skimage.color import label2rgb
import math

"""#สำหรับติดต่อกับ Google drive"""

from google.colab import drive
drive.mount('/content/drive')
#4/RwEp_GB8-piK_gxowM6kjNU0tAoI087r_yWJP_5zENzUG6cmxaqBMgI

"""#อ่านไฟล์รูปภาพ"""

img_path = 'drive/My Drive/Img pro/New Doc 2019-06-06 02.28.29_31.jpg'
im_gray = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
(thresh, im_bw) = cv2.threshold(im_gray, 128, 255, cv2.THRESH_BINARY | cv2.ADAPTIVE_THRESH_GAUSSIAN_C )
im_bw = ~im_bw
plt.imshow(im_bw, cmap='gray')

plt.imshow( skimage.io.imread(img_path))

new_im  = skimage.morphology.binary_opening(im_bw,skimage.morphology.disk(10))

plt.imshow(~new_im, cmap='gray')

label_image = skimage.morphology.label(~new_im)

plt.imshow(label_image, cmap='gray')

max = 0
board = None
regions = regionprops(label_image)
print(len(regions))
for region in regions:
  if (max < region.area):
    max = region.area
    board = region

img = cv2.imread(img_path)
onlymark = img[:,:,0]
(thresh, onlymark_im_bw) = cv2.threshold(onlymark, 128, 255, cv2.THRESH_BINARY | cv2.ADAPTIVE_THRESH_GAUSSIAN_C )
plt.imshow(onlymark_im_bw,cmap='gray')

r0, c0, r1, c1 = board.bbox
board_r = r1
board_c = c1
only_board = onlymark_im_bw[r0:r1, c0:c1]
plt.imshow(only_board,cmap='gray')

new_im_only_board  = skimage.morphology.binary_opening(only_board,skimage.morphology.disk(10))
new_im_only_board  = skimage.morphology.binary_closing(new_im_only_board,skimage.morphology.disk(5))
plt.imshow(new_im_only_board,cmap='gray')

label_only_board = skimage.morphology.label(new_im_only_board)

"""#คำนวนหาวงกลม"""

marks = []
regions = regionprops(label_only_board)
print(len(regions))
for region in regions:
    cir = (4*math.pi*region.area)/(math.pow(region.perimeter, 2))
    if(cir > 0.45): 
      print("circle ",cir)
      print("circle ",region.perimeter," ",region.area)
      marks.append(region)
    else: 
      print("squ ",cir)
      print("squ ",region.perimeter," ",region.area)

rstep = board_r/8
cstep = board_c/8
w, h = 8, 8;
board = [[0 for x in range(w)] for y in range(h)] 
for mark in marks:
  m_r ,m_c = mark.centroid
  r = int(round(m_r/rstep))
  c = int(round(m_c/cstep))
  if r>7 : r=7
  if c>7 : c = 7
  board[r][c] = 1

"""#แสดงผลลัพธ์"""

print('\n'.join([''.join(['{:4}'.format(item) for item in row]) 
      for row in board]))

